package com.svyd.wodes.repository.profile;

import com.svyd.wodes.app.project.home.page.profile.model.ProfileInfo;
import rx.Observable;

/**
 * Created by Svyd on 04.07.2016.
 */
public class StubProfileRepositoryImpl implements ProfileRepository {

    final String stubAvatar = "http://hdwallpaperbackgrounds.net/wp-content/uploads/2016/06/Beautiful-Girl-Wallpaper-6.jpg";

    private ProfileCache mCache;

    public StubProfileRepositoryImpl(ProfileCache cache) {
        mCache = cache;
    }

    @Override
    public Observable<ProfileInfo> getProfile() {
        return Observable.merge(getLocal(), getRemote().flatMap(info -> mCache.cacheProfile(info)));
    }

    Observable<ProfileInfo> getLocal() {
        return mCache.getProfile();
    }

    Observable<ProfileInfo> getRemote() {
        return Observable.create(subscriber -> {
            try {
                ProfileInfo.Builder builder = new ProfileInfo.Builder();
                Thread.sleep(2000);
                subscriber.onNext(builder
                        .setAvatar(stubAvatar)
                        .setName("Sofiya Ali")
                        .setExperience("Developer")
                        .build());
                subscriber.onCompleted();
            } catch (InterruptedException e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }
}
