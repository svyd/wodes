package com.svyd.wodes.repository.profile;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.svyd.wodes.app.application.WodesApplication;
import com.svyd.wodes.app.project.home.page.profile.model.ProfileInfo;

import rx.Observable;

/**
 * Created by Svyd on 04.07.2016.
 */
@SuppressWarnings("FieldCanBeLocal")
public class PreferenceProfileCacheImpl implements ProfileCache {

    private final String CACHE_PREFERENCES_NAME = "profile_cache";
    private final String PROFILE_CACHE_KEY = "profile_info";

    private SharedPreferences mPreferences;
    private Gson mGson;

    public PreferenceProfileCacheImpl() {
        mPreferences = WodesApplication
                .getContext()
                .getSharedPreferences(CACHE_PREFERENCES_NAME, Context.MODE_PRIVATE);

        mGson = new Gson();
    }

    @Override
    public Observable<ProfileInfo> getProfile() {
        return Observable.create(subscriber -> {
            try {
                String json = mPreferences.getString(PROFILE_CACHE_KEY, "");
                if (!json.equals("")) {
                    ProfileInfo info = mGson.fromJson(json, ProfileInfo.class);
                    subscriber.onNext(info);
                    subscriber.onCompleted();
                } else {
                    subscriber.unsubscribe();
                }
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<ProfileInfo> cacheProfile(ProfileInfo info) {
        return Observable.create(subscriber -> {
            try {
                String json = mGson.toJson(info);
                mPreferences.edit()
                        .putString(PROFILE_CACHE_KEY, json)
                        .commit();
                subscriber.onNext(info);
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }
}
