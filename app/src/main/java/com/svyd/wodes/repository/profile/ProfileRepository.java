package com.svyd.wodes.repository.profile;

import com.svyd.wodes.app.project.home.page.profile.model.ProfileInfo;

import rx.Observable;

/**
 * Created by Svyd on 04.07.2016.
 */
public interface ProfileRepository {
    Observable<ProfileInfo> getProfile();
}
