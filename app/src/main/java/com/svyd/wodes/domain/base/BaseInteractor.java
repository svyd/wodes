package com.svyd.wodes.domain.base;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by Svyd on 04.07.2016.
 */
public abstract class BaseInteractor {

    private Subscription mGetSubscription = Subscriptions.empty();

    @SuppressWarnings("unchecked")
    public void execute(Observer _subscriber) {
        mGetSubscription = buildObserver()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(_subscriber);
    }

    public void unSubscribe() {
        if (!mGetSubscription.isUnsubscribed())
            mGetSubscription.unsubscribe();
    }

    protected abstract Observable buildObserver();

}
