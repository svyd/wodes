package com.svyd.wodes.domain.profile;

import com.svyd.wodes.domain.base.BaseInteractor;
import com.svyd.wodes.repository.profile.ProfileRepository;

import rx.Observable;

/**
 * Created by Svyd on 04.07.2016.
 */
public class ProfileInteractor extends BaseInteractor {

    private ProfileRepository mRepository;

    public ProfileInteractor(ProfileRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildObserver() {
        return mRepository.getProfile();
    }
}
