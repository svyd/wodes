package com.svyd.wodes.app.base.ui;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.svyd.wodes.app.base.mvp.BasePresenter;
import com.svyd.wodes.app.base.tool.FragmentNavigator;
import com.svyd.wodes.app.base.tool.ToolbarManager;

/**
 * Created by Svyd on 02.07.2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private FragmentNavigator mFragmentNavigator;
    private ToolbarManager mToolbarManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initFields();
    }

    private void initFields() {
        mFragmentNavigator = new FragmentNavigator(this, getSupportFragmentManager(), getContainerId());
        mToolbarManager = new ToolbarManager(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (getPresenter() != null) {
            getPresenter().onStop();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (getPresenter() != null) {
            getPresenter().onStart();
        }
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        if (getToolbarId() == 0)
            return;

        Toolbar toolbar = (Toolbar) findViewById(getToolbarId());
        if (toolbar != null)
            setSupportActionBar(toolbar);
    }

    public FragmentNavigator getFragmentNavigator() {
        return mFragmentNavigator;
    }

    public ToolbarManager getToolbarManager() {
        return mToolbarManager;
    }

    public abstract BasePresenter getPresenter();

    //AbstractMethods
    public abstract
    @IdRes
    int getContainerId();

    public abstract
    @IdRes
    int getToolbarId();
}
