package com.svyd.wodes.app.base.mvp;

/**
 * Created by Svyd on 04.07.2016.
 */
public interface BaseView {
    void showProgress();
    void hideProgress();
}
