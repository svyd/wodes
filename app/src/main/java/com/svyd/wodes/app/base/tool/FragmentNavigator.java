package com.svyd.wodes.app.base.tool;

import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.svyd.wodes.app.base.ui.BaseActivity;
import com.svyd.wodes.app.base.ui.BaseFragment;

/**
 * Created by Svyd on 02.07.2016.
 */
public class FragmentNavigator {

    protected FragmentManager mFragmentManager;
    private BaseActivity mActivity;

    protected
    @IdRes
    int mContainerId = -1;

    public FragmentNavigator(BaseActivity _activity, FragmentManager _manager, @IdRes int _containerId) {
        mFragmentManager = _manager;
        mContainerId = _containerId;
        mActivity = _activity;
    }

    protected FragmentTransaction getTransaction() {
        return mFragmentManager.beginTransaction();
    }

    public void clearBackStack() {
        int entryCount = mFragmentManager.getBackStackEntryCount();
        if (entryCount <= 0)
            return;

        FragmentManager.BackStackEntry entry = mFragmentManager.getBackStackEntryAt(0);
        int id = entry.getId();
        mActivity.runOnUiThread(() ->
                mFragmentManager.popBackStackImmediate(id, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        );

    }

    public void replaceFragmentWithoutBackStack(BaseFragment _fragment) {
        clearBackStack();
        getTransaction()
                .replace(mContainerId, _fragment)
                .commit();
    }

    public void addFragmentWithoutBackStack(BaseFragment _fragment) {
        clearBackStack();
        getTransaction()
                .add(mContainerId, _fragment)
                .commit();
    }

    public void addFragmentWithContainer(BaseFragment _fragment, @IdRes int _containerId) {
        getTransaction()
                .replace(_containerId, _fragment)
                .commit();
    }

    public void replaceFragmentWithBackStack(BaseFragment _fragment, @IdRes int _containerId) {
        getTransaction()
//                .setCustomAnimations(R.anim.fadein, R.anim.fadeout, R.anim.fadein, R.anim.fadeout)
                .replace(_containerId, _fragment)
                .addToBackStack(_fragment.getClass().getSimpleName())
                .commit();
    }

    public void replaceFragmentWithBackStack(BaseFragment _fragment) {
        getTransaction()
//                .setCustomAnimations(R.anim.fadein, R.anim.fadeout, R.anim.fadein, R.anim.fadeout)
                .replace(mContainerId, _fragment)
                .addToBackStack(_fragment.getClass().getSimpleName())
                .commit();
    }

    public void addFragmentWithBackStack(BaseFragment _fragment) {
        getTransaction()
//                .setCustomAnimations(R.anim.fadein, R.anim.fadeout, R.anim.fadein, R.anim.fadeout)
                .add(mContainerId, _fragment)
                .addToBackStack(_fragment.getClass().getSimpleName())
                .commit();
    }

    public void popBackStack() {
        mFragmentManager.popBackStack();
    }

    public BaseFragment getTopFragment() {
        return (BaseFragment) mFragmentManager.findFragmentById(mContainerId);
    }

    public BaseFragment getTopFragment(@IdRes int _containerId) {
        return (BaseFragment) mFragmentManager.findFragmentById(_containerId);
    }
}
