package com.svyd.wodes.app.application;

import android.app.Application;
import android.content.Context;

/**
 * Created by Svyd on 02.07.2016.
 */
public class WodesApplication extends Application {

    private static WodesApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;
    }

    public static Context getContext() {
        return sInstance;
    }

}
