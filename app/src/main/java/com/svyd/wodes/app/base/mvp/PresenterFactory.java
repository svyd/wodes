package com.svyd.wodes.app.base.mvp;

import android.os.Bundle;

import com.svyd.wodes.app.base.mvp.BasePresenter;

/**
 * Created by Svyd on 04.07.2016.
 */
public interface PresenterFactory<T extends BasePresenter, V extends BaseView> {
    T providePresenter(Bundle args, V view);
}
