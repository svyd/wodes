package com.svyd.wodes.app.project.home.activity.contract;

import com.svyd.wodes.app.base.mvp.BasePresenter;

/**
 * Created by Svyd on 02.07.2016.
 */
public interface HomePresenter extends BasePresenter {
    void onPageScrolled(int position, float positionOffset, int positionOffsetPixels);
    void onPageSelected(int position);
    void onPageScrollStateChanged(int state);
    void onCheckedChanged(int checkedId);
}
