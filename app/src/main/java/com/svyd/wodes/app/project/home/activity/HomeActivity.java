package com.svyd.wodes.app.project.home.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.RadioGroup;

import com.svyd.wodes.R;
import com.svyd.wodes.app.base.mvp.BasePresenter;
import com.svyd.wodes.app.base.ui.BaseActivity;
import com.svyd.wodes.app.project.home.activity.adapter.HomePageAdapter;
import com.svyd.wodes.app.project.home.activity.contract.HomePresenter;
import com.svyd.wodes.app.project.home.activity.contract.HomeView;
import com.svyd.wodes.app.project.home.page.profile.fragment.ProfileFragment;
import com.svyd.wodes.app.project.home.view.CenteredRadioButton;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Svyd on 02.07.2016.
 */
public class HomeActivity extends BaseActivity
        implements HomeView,
        ViewPager.OnPageChangeListener,
        RadioGroup.OnCheckedChangeListener {

    @BindView(R.id.rbMessage_IBB)
    CenteredRadioButton rbMessage;

    @BindView(R.id.rbDelivery_IBB)
    CenteredRadioButton rbDelivery;

    @BindView(R.id.rbProfile_IBB)
    CenteredRadioButton rbProfile;

    @BindView(R.id.rbPresent_IBB)
    CenteredRadioButton rbPresent;

    @BindView(R.id.rbHelp_IBB)
    CenteredRadioButton rbHelp;

    @BindView(R.id.vpHome_AH)
    ViewPager vpHome;

    @BindView(R.id.bbHome_AC)
    RadioGroup rgMenu;

    private HomePresenter mPresenter;
    private HomePageAdapter mAdapter;

    private final int MODE_LEFT = -1;
    private final int MODE_RIGHT = 1;

    private int mSwipeDirection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        initPresenter();
        ButterKnife.bind(this);
        initPager();
    }

    private void initPresenter() {
        mPresenter = new HomePresenterImpl(this);
    }

    private void initPager() {
        rgMenu.setOnCheckedChangeListener(this);
        mAdapter = new HomePageAdapter(getSupportFragmentManager());
        vpHome.addOnPageChangeListener(this);
        vpHome.setOffscreenPageLimit(4);

        mAdapter.addFragment(ProfileFragment.newInstance());
        mAdapter.addFragment(ProfileFragment.newInstance());
        mAdapter.addFragment(ProfileFragment.newInstance());
        mAdapter.addFragment(ProfileFragment.newInstance());
        mAdapter.addFragment(ProfileFragment.newInstance());

        vpHome.setAdapter(mAdapter);
        vpHome.setCurrentItem(2, false);
    }

    @Override
    public BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public int getContainerId() {
        return 0;
    }

    @Override
    public int getToolbarId() {
        return 0;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        mPresenter.onPageScrolled(position, positionOffset, positionOffsetPixels);
    }

    @Override
    public void onPageSelected(int position) {
        mPresenter.onPageSelected(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        mPresenter.onPageScrollStateChanged(state);
    }

    @Override
    public void scrollToFragment(int position) {
        vpHome.setCurrentItem(position, true);
    }

    @Override
    public void checkButton(int index) {
        switch (index) {
            case 0:
                rbMessage.setChecked(true);
                break;
            case 1:
                rbDelivery.setChecked(true);
                break;
            case 2:
                rbProfile.setChecked(true);
                break;
            case 3:
                rbPresent.setChecked(true);
                break;
            case 4:
                rbHelp.setChecked(true);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        mPresenter.onCheckedChanged(checkedId);
    }
}
