package com.svyd.wodes.app.project.home.activity.contract;

/**
 * Created by Svyd on 02.07.2016.
 */
public interface HomeView {
    void scrollToFragment(int position);
    void checkButton(int index);
}
