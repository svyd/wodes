package com.svyd.wodes.app.project.home.page.profile.contract;

import com.svyd.wodes.app.base.mvp.BaseView;

/**
 * Created by Svyd on 04.07.2016.
 */
public interface ProfileView extends BaseView {
    void setAvatar(String url);
    void setName(String name);
    void setExperience(String level);
    void setListData();
    void showToast(String toast);
}
