package com.svyd.wodes.app.project.home.page.profile.presenter.implementation;

import com.svyd.wodes.app.project.home.page.profile.contract.ProfilePresenter;
import com.svyd.wodes.app.project.home.page.profile.contract.ProfileView;
import com.svyd.wodes.app.project.home.page.profile.model.ProfileInfo;
import com.svyd.wodes.domain.profile.ProfileInteractor;

import rx.Observer;

/**
 * Created by Svyd on 04.07.2016.
 */
public class MyProfilePresenterImpl implements ProfilePresenter {

    private ProfileInteractor mProfileInteractor;
    private ProfileView mView;

    public MyProfilePresenterImpl(ProfileInteractor interactor, ProfileView view) {
        mProfileInteractor = interactor;
        mView = view;
    }

    @Override
    public void initialize() {
        getProfile();
    }

    private void getProfile() {
        mProfileInteractor.execute(new ProfileObserver());
        mView.showProgress();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mProfileInteractor.unSubscribe();
    }

    private void setUpView(ProfileInfo info) {
        mView.setName(info.getName());
        mView.setAvatar(info.getAvatar());
        mView.setExperience(info.getExperience());
        mView.setListData();
    }

    @Override
    public void onFabClick() {
        getProfile();
    }

    private class ProfileObserver implements Observer<ProfileInfo> {

        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
            mView.hideProgress();
            mView.showToast(e.getMessage());
        }

        @Override
        public void onNext(ProfileInfo profileInfo) {
            setUpView(profileInfo);
            mView.hideProgress();
        }
    }
}
