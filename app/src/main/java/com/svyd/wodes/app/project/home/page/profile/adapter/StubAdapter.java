package com.svyd.wodes.app.project.home.page.profile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.svyd.wodes.R;

/**
 * Created by Svyd on 04.07.2016.
 */
public class StubAdapter extends RecyclerView.Adapter<StubAdapter.ProfileHolder> {

    private Context mContext;

    public StubAdapter(Context context) {
        mContext = context;
    }

    @Override
    public ProfileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProfileHolder(LayoutInflater.from(mContext).inflate(R.layout.item_profile_details, parent, false));
    }

    @Override
    public void onBindViewHolder(ProfileHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 4;
    }

    class ProfileHolder extends RecyclerView.ViewHolder {

        public ProfileHolder(View itemView) {
            super(itemView);
        }
    }
}
