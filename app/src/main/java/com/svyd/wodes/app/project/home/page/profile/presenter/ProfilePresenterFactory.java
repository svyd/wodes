package com.svyd.wodes.app.project.home.page.profile.presenter;

import android.os.Bundle;

import com.svyd.wodes.app.base.mvp.PresenterFactory;
import com.svyd.wodes.app.project.home.page.profile.contract.ProfilePresenter;
import com.svyd.wodes.app.project.home.page.profile.contract.ProfileView;
import com.svyd.wodes.app.project.home.page.profile.presenter.implementation.MyProfilePresenterImpl;
import com.svyd.wodes.domain.profile.ProfileInteractor;
import com.svyd.wodes.repository.profile.PreferenceProfileCacheImpl;
import com.svyd.wodes.repository.profile.StubProfileRepositoryImpl;

/**
 * Created by Svyd on 04.07.2016.
 */
public class ProfilePresenterFactory implements PresenterFactory<ProfilePresenter, ProfileView> {

    @Override
    public ProfilePresenter providePresenter(Bundle args, ProfileView view) {
        return new MyProfilePresenterImpl(
                new ProfileInteractor(
                        new StubProfileRepositoryImpl(
                                new PreferenceProfileCacheImpl())), view);
    }
}
