package com.svyd.wodes.app.base.mvp;

/**
 * Created by Svyd on 02.07.2016.
 */
public interface BasePresenter {
    void initialize();
    void onStart();
    void onStop();
}
