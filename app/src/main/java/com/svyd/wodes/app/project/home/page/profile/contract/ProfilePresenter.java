package com.svyd.wodes.app.project.home.page.profile.contract;

import com.svyd.wodes.app.base.mvp.BasePresenter;

/**
 * Created by Svyd on 04.07.2016.
 */
public interface ProfilePresenter extends BasePresenter {
    void onFabClick();
}
