package com.svyd.wodes.app.project.home.page.profile.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.svyd.wodes.R;
import com.svyd.wodes.app.base.mvp.BasePresenter;
import com.svyd.wodes.app.base.mvp.PresenterFactory;
import com.svyd.wodes.app.base.ui.BaseFragment;
import com.svyd.wodes.app.project.home.page.profile.adapter.StubAdapter;
import com.svyd.wodes.app.project.home.page.profile.contract.ProfilePresenter;
import com.svyd.wodes.app.project.home.page.profile.contract.ProfileView;
import com.svyd.wodes.app.project.home.page.profile.presenter.implementation.MyProfilePresenterImpl;
import com.svyd.wodes.app.project.home.page.profile.presenter.ProfilePresenterFactory;
import com.svyd.wodes.domain.profile.ProfileInteractor;
import com.svyd.wodes.repository.profile.StubProfileRepositoryImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Svyd on 02.07.2016.
 */
public class ProfileFragment extends BaseFragment implements ProfileView {

    @BindView(R.id.clContent_FP)
    CoordinatorLayout clContent;

    @BindView(R.id.appbar_FP)
    AppBarLayout appbar;

    @BindView(R.id.collapsing_toolbar_FP)
    CollapsingToolbarLayout collapsingToolbar;

    @BindView(R.id.ivAvatar_FP)
    ImageView ivAvatar;

    @BindView(R.id.toolbar_FP)
    Toolbar toolbar;

    @BindView(R.id.rvProfile_FP)
    RecyclerView rvProfile;

    @BindView(R.id.fabEdit_FP)
    FloatingActionButton fabEdit;

    @BindView(R.id.tvExperience_FP)
    TextView tvExperience;

    @BindView(R.id.pbProfile_FP)
    ProgressBar pbProfile;

    private ProfilePresenter mPresenter;
    private PresenterFactory<ProfilePresenter, ProfileView> mFactory;

    public static ProfileFragment newInstance() {

        Bundle args = new Bundle();

        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        providePresenter();
        initList();
        initPresenter();
    }

    private void providePresenter() {
        mFactory = new ProfilePresenterFactory();
        mPresenter = mFactory.providePresenter(getArguments(), this);
    }

    void initPresenter() {
        mPresenter.initialize();
    }

    void initList() {
        StubAdapter adapter = new StubAdapter(getActivity());
        rvProfile.setLayoutManager(new LinearLayoutManager(rvProfile.getContext()));
        rvProfile.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void setAvatar(String url) {
        Glide.with(getActivity())
                .load(url)
                .fitCenter()
                .centerCrop()
                .animate(android.R.anim.fade_in)
                .into(ivAvatar);
    }

    @Override
    public void setName(String name) {
        collapsingToolbar.setTitle(name);
    }

    @Override
    public void setExperience(String level) {
        tvExperience.setText(String.format("%s%s", getString(R.string.text_experience_FP), level));
    }

    @Override
    public void setListData() {
        rvProfile.setVisibility(View.VISIBLE);
    }

    @Override
    public void showToast(String toast) {
        Toast.makeText(getActivity(), toast, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        pbProfile.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbProfile.setVisibility(View.GONE);
    }

    @OnClick(R.id.fabEdit_FP)
    void onFabClick() {
        mPresenter.onFabClick();
    }
}
