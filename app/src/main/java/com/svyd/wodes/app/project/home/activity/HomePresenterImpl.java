package com.svyd.wodes.app.project.home.activity;

import android.util.Log;

import com.svyd.wodes.R;
import com.svyd.wodes.app.project.home.activity.contract.HomePresenter;
import com.svyd.wodes.app.project.home.activity.contract.HomeView;

/**
 * Created by Svyd on 02.07.2016.
 */
public class HomePresenterImpl implements HomePresenter {

    private HomeView mView;

    public HomePresenterImpl(HomeView view) {
        mView = view;
    }

    @Override
    public void initialize() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        mView.checkButton(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        Log.d("HomePresenter", "onPageScrollStateChanged() called with: " + "state = [" + state + "]");
    }

    @Override
    public void onCheckedChanged(int checkedId) {
        changeTab(checkedId);
    }

    private void changeTab(int checkedId) {
        switch (checkedId) {
            case R.id.rbMessage_IBB:
                mView.scrollToFragment(0);
                break;
            case R.id.rbDelivery_IBB:
                mView.scrollToFragment(1);
                break;
            case R.id.rbProfile_IBB:
                mView.scrollToFragment(2);
                break;
            case R.id.rbPresent_IBB:
                mView.scrollToFragment(3);
                break;
            case R.id.rbHelp_IBB:
                mView.scrollToFragment(4);
                break;
        }
    }
}
