package com.svyd.wodes.app.base.ui;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.svyd.wodes.app.base.mvp.BasePresenter;
import com.svyd.wodes.app.base.tool.FragmentNavigator;
import com.svyd.wodes.app.base.tool.ToolbarManager;

/**
 * Created by Svyd on 02.07.2016.
 */
public abstract class BaseFragment extends Fragment {

    protected @LayoutRes
    int mContentId = -1;

    protected void setContentView(@LayoutRes int _contentId) {
        mContentId = _contentId;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getPresenter() != null) {
            getPresenter().onStart();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (getPresenter() != null) {
            getPresenter().onStop();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mContentId == -1)
            throw new RuntimeException("You need to call setContentView before onCreateView");
        return inflater.inflate(mContentId, container, false);
    }

    protected abstract BasePresenter getPresenter();

    protected FragmentNavigator getFragmentNavigator() {
        return ((BaseActivity) getActivity()).getFragmentNavigator();
    }

    protected ToolbarManager getToolbarManager() {
        return ((BaseActivity) getActivity()).getToolbarManager();
    }

}
