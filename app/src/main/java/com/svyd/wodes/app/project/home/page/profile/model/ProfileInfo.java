package com.svyd.wodes.app.project.home.page.profile.model;

/**
 * Created by Svyd on 04.07.2016.
 */
public class ProfileInfo {

    private String name;
    private String avatar;
    private String experience;

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getExperience() {
        return experience;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getName() {
        return name;
    }

    public static class Builder {
        private String name;
        private String avatar;
        private String experience;

        public Builder setExperience(String experience) {
            this.experience = experience;
            return this;
        }

        public Builder setAvatar(String avatar) {
            this.avatar = avatar;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public ProfileInfo build() {
            ProfileInfo info = new ProfileInfo();

            info.setAvatar(avatar);
            info.setName(name);
            info.setExperience(experience);

            return info;
        }
    }
}
